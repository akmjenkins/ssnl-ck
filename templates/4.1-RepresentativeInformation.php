<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section class="excerpt-block">
		<div class="sw">
			
			<div>

				<div class="hgroup centered">
					<h1 class="hgroup-title">SSNL Membership Registration</h1>
				</div><!-- .hgroup.centered -->

				<p class="excerpt">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
					Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar 
					tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
					Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien 
					nunc eget odio.
				</p>				
			</div>


		</div><!-- .sw -->
	</section>

	<section class="light-bg">
		<div class="sw">


			<form action="/" novalidate class="body-form full">

				<div class="registration-form">

					<div class="fieldset">

						<div class="hgroup section-header">
							<h3 class="hgroup-title">Representative Information</h3>
						</div><!-- .hgroup -->

						<div class="progress-bar">
							<span>School Information</span>
							<span class="selected">Representative Information</span>
							<span>Coach &amp; Sports</span>
							<span>Payment</span>
						</div><!-- .progress-bar -->

						<fieldset>
							<legend>Principal's Information</legend>

							<div class="grid collapse-599">

								<div class="col col-2">
									<div class="item">
										<input type="text" name="principal_name" placeholder="Principal's Name">
									</div><!-- .item -->
								</div><!-- .col -->

								<div class="col col-2">
									<div class="item">
										<input type="email" name="principal_email" placeholder="Principal's Email">
									</div><!-- .item -->
								</div><!-- .col -->	

								<div class="col col-1">
									<div class="item">
										<label class="checkbox">
											<input type="checkbox" name="principal_approval">
											<span>I verify that the above named Principal has approved registering this school for SSNL Membership.</span>
										</label>
									</div><!-- .item -->
								</div><!-- .col -->	

							</div><!-- .grid -->
						</fieldset>

						<fieldset>
							<legend>Athletic Director's Information</legend>

							<div class="grid collapse-599">
								<div class="col col-2">
									<div class="item">
										<h6>Current Athletic Director</h6>

										<div class="athletic-director">
											<span class="block">Director Name</span>
											<span class="block">director@theiremail.com</span>

											<div class="athletic-director-actions">
												<span class="form-action form-action-remove">Remove</span>
												<span class="form-action form-action-password">Change Password</span>
											</div><!-- .atheletic-director-actions -->

										</div><!-- .athletic-director -->
									</div>
								</div>
								<div class="col col-2">
									<div class="item">
										<h6>Add New Athletic Director</h6>
										<div class="grid pad10">
											<div class="col col-1">
												<div class="item">
													<input type="text" name="new_principal_name" placeholder="Principal's Name">
												</div><!-- .item -->
											</div><!-- .col -->
											<div class="col col-1">
												<div class="item">
													<input type="email" name="new_principal_email" placeholder="Principal's Email">
												</div><!-- .item -->
											</div><!-- .col -->
											<div class="col col-1">
												<div class="item">
													<button type="button" class="button fill primary">Add</button>
												</div><!-- .item -->
											</div><!-- .col -->
										</div><!-- .grid -->

									</div><!-- .item -->
								</div><!-- .col -->
							</div><!-- .grid -->
						</fieldset>

						<fieldset>
							<legend>Login Information</legend>

							<div class="grid collapse-599">

								<div class="col col-2">
									<div class="item">
										<input type="text" name="login_principal_name" placeholder="Principal's Name">
									</div><!-- .item -->
								</div><!-- .col -->

								<div class="col col-2">
									<div class="item">
										<input type="email" name="login_principal_email" placeholder="Principal's Email">
									</div><!-- .item -->
								</div><!-- .col -->	

							</div><!-- .grid -->
						</fieldset>

						<div class="form-controls">
							<button type="button" class="button fill previous">Previous</button>
							<button type="button" class="button fill primary next">Next</button>
						</div><!-- .form-controls -->

					</div><!-- .fieldset -->
				
				</div><!-- .registration-form -->
	
			</form>
			

		</div><!-- .sw -->
	</section><!-- .light-bg -->

	<section class="d-bg primary-bg">
		<div class="sw">
			
			<?php include('inc/i-latest-tweet.php'); ?>

		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>