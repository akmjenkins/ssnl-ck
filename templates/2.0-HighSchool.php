<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">

	<div class="hero-img lazybg" data-src="../assets/images/temp/hero/hero-1.jpg"></div>

	<div class="hero-profile-links">
		
		<div class="hero-profile lazybg" data-src="../assets/images/temp/temp-1.jpg">
			<div class="hero-profile-content">
				<span class="hero-profile-name">Person's Name</span>

				<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
					Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
					Proin sodales pulvinar tempor.
				</p>

				<a href="#" class="button fill primary">Read More</a>
			</div><!-- .hero-profile-content -->
		</div><!-- .hero-profile -->

		<div class="hero-links">
			<ul>
				<li><a href="#">Tournament Registration</a></li>
				<li><a href="#">Sponsors</a></li>
				<li><a href="#">Links</a></li>
				<li><a href="#">FAQs</a></li>
			</ul>
		</div><!-- .hero-links -->

	</div><!-- .hero-profile -->

</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
			
			<div class="hgroup centered section-header">
				<h2 class="hgroup-title">Latest Updates</h2>
			</div><!-- .hgroup -->

			<div class="grid centered">

				<div class="col col-3 md-col-2 sm-col-1">
					<div class="item">
						
						<h5>Upcoming Tournaments</h5>
						<div class="block-with-time">

							<time datetime="2015-02-20">
								<span class="m-d">Feb 20</span>
								<span class="y">2015</span>
							</time>

							<div class="content">
								<a href="#" class="title">Provincial Table Tennis Championship</a>
								<span>Dorset Collegiate | Pelley's Island</span>
							</div><!-- .content -->

							<a href="#" class="button primary fill">All Tournaments</a>

						</div><!-- .block-with-time -->

					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col col-3 md-col-2 sm-col-1">
					<div class="item">
						
						<h5>Latest From Twitter</h5>
						<div class="block-with-time">

							<time datetime="2015-02-20">
								<span class="m-d">Feb 20</span>
								<span class="y">2015</span>
							</time>

							<div class="content">
								<p>
									Nulla in orci viverra velit convallis feugiat. Donec pharetra lacus a sem 
									vulputate viverra vel in ante. Sed dolor libero, tristique in sed.
								</p>
							</div>

							<a href="#" class="button primary fill">Follow Us</a>							

						</div><!-- .block-with-time -->

					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col col-3 md-col-2 sm-col-1">
					<div class="item">
						
						<h5>Latest News</h5>
						<div class="block-with-time">

							<time datetime="2015-02-20">
								<span class="m-d">Feb 20</span>
								<span class="y">2015</span>
							</time>

							<div class="content">
								<a href="#" class="title">Provincial Table Tennis Championship</a>
								<span>Dorset Collegiate | Pelley's Island</span>
							</div><!-- .content -->

							<a href="#" class="button primary fill">All News</a>							

						</div><!-- .block-with-time -->

					</div><!-- .item -->
				</div><!-- .col -->

			</div><!-- .grid -->

		</div><!-- .sw -->
	</section>

	<section class="d-bg secondary-bg">
		<div class="sw">
			
			<div class="hgroup centered section-header">
				<h2 class="hgroup-title">Sports Stars Standings</h2>
			</div><!-- .hgroup -->

			<div class="grid eqh">

				<div class="col col-4 md-col-2 xs-col-1">
					<div class="item article-body light-bg">
						<div class="pad-20">
						
							<h5>A Classification</h5>

							<ol>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
							</ol>

						</div><!-- .pad-20 -->
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col col-4 md-col-2 xs-col-1">
					<div class="item article-body light-bg">
						<div class="pad-20">
						
							<h5>2A Classification</h5>

							<ol>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
							</ol>

						</div><!-- .pad-20 -->
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col col-4 md-col-2 xs-col-1">
					<div class="item article-body light-bg">
						<div class="pad-20">
						
							<h5>3A Classification</h5>

							<ol>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
							</ol>

						</div><!-- .pad-20 -->
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col col-4 md-col-2 xs-col-1">
					<div class="item article-body light-bg">
						<div class="pad-20">
						
							<h5>4A Classification</h5>

							<ol>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
								<li>School Name</li>
							</ol>

						</div><!-- .pad-20 -->
					</div><!-- .item -->
				</div><!-- .col -->

			</div><!-- .grid -->

		</div><!-- .sw -->
	</section>

	<section class="d-bg primary-bg">
		<div class="sw">
			
			<?php include('inc/i-latest-tweet.php'); ?>

		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>