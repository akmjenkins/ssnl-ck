<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section class="excerpt-block">
		<div class="sw">
			
			<div>

				<div class="hgroup centered">
					<h1 class="hgroup-title">Sports</h1>
				</div><!-- .hgroup.centered -->

				<p class="excerpt">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
					Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar 
					tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
					Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien 
					nunc eget odio.
				</p>				
			</div>


		</div><!-- .sw -->
	</section>

	<section class="light-bg">
		<div class="sw">

				<div class="hgroup centered section-header">
					<h2 class="hgroup-title">March 2015</h2>
				</div><!-- .hgroup.centered -->

			<div class="calendar-buttons">
				
				<a href="#" class="button primary fill">Back to Sports Page</a>

				<div class="calendar-selectors">

					<div class="selector with-arrow">
						<select>
							<option value="">Select Month</option>
							<option value="1">January</option>
							<option value="2">February</option>
							<option value="3">March</option>
							<option value="4">April</option>
							<option value="5">May</option>
							<option value="6">June</option>
							<option value="7">July</option>
							<option value="8">August</option>
							<option value="9">September</option>
							<option value="10">October</option>
							<option value="11">November</option>
							<option value="12">December</option>
						</select>
						<span class="value"></span>
					</div><!-- .selector -->

					<div class="selector with-arrow">
						<select>
							<option value="">Select Sport</option>
							<option value="1">January</option>
							<option value="2">February</option>
							<option value="3">March</option>
							<option value="4">April</option>
							<option value="5">May</option>
							<option value="6">June</option>
							<option value="7">July</option>
							<option value="8">August</option>
							<option value="9">September</option>
							<option value="10">October</option>
							<option value="11">November</option>
							<option value="12">December</option>
						</select>
						<span class="value"></span>
					</div><!-- .selector -->

				</div><!-- .calendar-selectors -->

			</div><!-- .calendar-buttons -->

			<div class="calendar-mv-calendar">
				
				<div class="calendar-mv-calendar-head">
					<span>Sun</span>
					<span>Mon</span>
					<span>Tue</span>
					<span>Wed</span>
					<span>Thu</span>
					<span>Fri</span>
					<span>Sat</span>
				</div><!-- .calendar-mv-calendar-head -->

				<div class="calendar-mv-calendar-body">

					<div>
						<span class="day">1</span>
					</div>

					<div>
						<span class="day">2</span>
					</div>

					<div>
						<span class="day">3</span>
					</div>

					<div>
						<span class="day">4</span>
					</div>

					<div>
						<span class="day">5</span>
					</div>

					<div>
						<span class="day">6</span>

						<a href="#" class="inline event">Sr. High BasketBall Tournament</a>
						<a href="#" class="inline event">Jr. High BasketBall Tournament</a>

						<a href="#" class="more">More</a>

					</div>

					<div>
						<span class="day">7</span>

						<a href="#" class="inline event">Sr. High BasketBall Tournament</a>
						<a href="#" class="inline event">Jr. High BasketBall Tournament</a>

						<a href="#" class="more">More</a>

					</div>

					<div>
						<span class="day">8</span>

						<a href="#" class="inline event">Sr. High BasketBall Tournament</a>
						<a href="#" class="inline event">Jr. High BasketBall Tournament</a>

						<a href="#" class="more">More</a>

					</div>

					<div>
						<span class="day">9</span>
					</div>

					<div>
						<span class="day">10</span>
					</div>

					<div>
						<span class="day">11</span>
					</div>

					<div>
						<span class="day">12</span>
					</div>

					<div>
						<span class="day">13</span>
					</div>

					<div>
						<span class="day">14</span>
					</div>

					<div>
						<span class="day">15</span>
					</div>

					<div>
						<span class="day">16</span>
					</div>

					<div>
						<span class="day">17</span>
					</div>

					<div>
						<span class="day">18</span>
					</div>

					<div>
						<span class="day">19</span>
					</div>

					<div>
						<span class="day">20</span>
					</div>

					<div>
						<span class="day">21</span>
					</div>

					<div>
						<span class="day">22</span>
					</div>

					<div>
						<span class="day">23</span>
					</div>

					<div>
						<span class="day">24</span>
					</div>

					<div>
						<span class="day">25</span>
					</div>

					<div>
						<span class="day">26</span>
					</div>

					<div>
						<span class="day">27</span>
					</div>

					<div>
						<span class="day">28</span>
					</div>

					<div>
						<span class="day">29</span>
					</div>

					<div>
						<span class="day">30</span>
					</div>

					<div>
						<span class="day">31</span>
					</div>

					<div class="outside-month">
						<span class="day">Apr 1</span>
					</div>

					<div class="outside-month">
						<span class="day">2</span>
					</div>

					<div class="outside-month">
						<span class="day">3</span>
					</div>

					<div class="outside-month">
						<span class="day">4</span>
					</div>

				</div><!-- .caleandar-mv-calendar-body -->

			</div><!-- .calendar-month-view-calendar -->


		</div><!-- .sw -->
	</section>

	<section class="d-bg primary-bg">
		<div class="sw">
			
			<?php include('inc/i-latest-tweet.php'); ?>

		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>