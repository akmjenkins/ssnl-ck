<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero-swiper swiper-wrapper">
	<div class="swiper"
		data-dots="true"
		data-arrows="false"
		data-fade="true"
		data-autoplay="true"
		data-autoplay-speed="5000"
		data-pause-on-hover="false"
		data-update-lazy-images="true">

		<div class="swipe-item">
			<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-soccer.jpg"></div>
		</div><!-- .swipe-item -->

		<div class="swipe-item">
			<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-soccer-i.jpg"></div>
		</div><!-- .swipe-item -->

	</div><!-- .swiper -->
</div><!-- .hero-swiper -->

<div class="body">

	<section class="excerpt-block">
		<div class="sw">
			
			<div>

				<div class="hgroup centered">
					<h1 class="hgroup-title">Outdoor Soccer</h1>
				</div><!-- .hgroup.centered -->

				<p class="excerpt">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
					Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar 
					tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
					Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien 
					nunc eget odio.
				</p>

				<div class="selector with-arrow">
					<select>
						<option value="">Select A Sport</option>
						<option value="1" data-tag="Slopitch">Slopitch</option>
						<option value="1" data-tag="Outdoor Soccer">Outdoor Soccer</option>
						<option value="1" data-tag="Cross-Country Running">Cross-Country Running</option>
						<option value="1" data-tag="Indoor Soccer">Indoor Soccer</option>
						<option value="1" data-tag="Volleyball">Volleyball</option>
						<option value="1" data-tag="Table Tennis">Table Tennis</option>
						<option value="1" data-tag="Cross-Country Skiing">Cross-Country Skiing</option>
						<option value="1" data-tag="Wrestling">Wrestling</option>
						<option value="1" data-tag="Basketball">Basketball</option>
					</select>
					<span class="value"></span>
				</div><!-- .selector -->
			</div>


		</div><!-- .sw -->
	</section>

	<section class="light-bg">
		<div class="sw">

			<div class="main-body">

				<div class="content">

					<div class="tournament-list">

						<h4>Zone Tournaments</h4>

						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
							Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. 
							Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, 
							nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.
						</p>

						<span class="tournament-list-title">St. John's</span>

						<div class="calendar-month-row">
							<div class="calendar-month-time">
								<time>
									<span class="m">Jan</span>
									<span class="d">03</span>
									<span class="y">2015</span>
								</time>
							</div><!-- .calendar-month-time -->
							<div class="calendar-month-title">
								<span class="block">2A Boys Outdoor Soccer</span>
							</div><!-- .calendar-month-title -->
							<div class="calendar-month-loc">
								<span class="block">Mount Pearl Sr. High</span>
							</div><!-- .calendar-month-loc -->
							<div class="calendar-month-button">
								<a href="#" class="button fill primary small">Register</a>
							</div><!-- .calendar-month-button -->
						</div><!-- .calendar-month-row -->

						<div class="calendar-month-row">
							<div class="calendar-month-time">
								<time>
									<span class="m">Jan</span>
									<span class="d">03</span>
									<span class="y">2015</span>
								</time>
							</div><!-- .calendar-month-time -->
							<div class="calendar-month-title">
								<span class="block">2A Boys Outdoor Soccer</span>
							</div><!-- .calendar-month-title -->
							<div class="calendar-month-loc">
								<span class="block">Mount Pearl Sr. High</span>
							</div><!-- .calendar-month-loc -->
							<div class="calendar-month-button">
								<a href="#" class="button fill primary small">Register</a>
							</div><!-- .calendar-month-button -->
						</div><!-- .calendar-month-row -->

						<div class="calendar-month-row">
							<div class="calendar-month-time">
								<time>
									<span class="m">Jan</span>
									<span class="d">03</span>
									<span class="y">2015</span>
								</time>
							</div><!-- .calendar-month-time -->
							<div class="calendar-month-title">
								<span class="block">2A Boys Outdoor Soccer</span>
							</div><!-- .calendar-month-title -->
							<div class="calendar-month-loc">
								<span class="block">Mount Pearl Sr. High</span>
							</div><!-- .calendar-month-loc -->
							<div class="calendar-month-button">
								<a href="#" class="button fill primary small">Register</a>
							</div><!-- .calendar-month-button -->
						</div><!-- .calendar-month-row -->

						<div class="calendar-month-row">
							<div class="calendar-month-time">
								<time>
									<span class="m">Jan</span>
									<span class="d">03</span>
									<span class="y">2015</span>
								</time>
							</div><!-- .calendar-month-time -->
							<div class="calendar-month-title">
								<span class="block">2A Boys Outdoor Soccer</span>
							</div><!-- .calendar-month-title -->
							<div class="calendar-month-loc">
								<span class="block">Mount Pearl Sr. High</span>
							</div><!-- .calendar-month-loc -->
							<div class="calendar-month-button">
								<a href="#" class="button fill primary small">Register</a>
							</div><!-- .calendar-month-button -->
						</div><!-- .calendar-month-row -->

						<div class="calendar-month-row">
							<div class="calendar-month-time">
								<time>
									<span class="m">Jan</span>
									<span class="d">03</span>
									<span class="y">2015</span>
								</time>
							</div><!-- .calendar-month-time -->
							<div class="calendar-month-title">
								<span class="block">2A Boys Outdoor Soccer</span>
							</div><!-- .calendar-month-title -->
							<div class="calendar-month-loc">
								<span class="block">Mount Pearl Sr. High</span>
							</div><!-- .calendar-month-loc -->
							<div class="calendar-month-button">
								<a href="#" class="button fill primary small">Register</a>
							</div><!-- .calendar-month-button -->
						</div><!-- .calendar-month-row -->

						<span class="tournament-list-title">Avalon</span>

						<div class="calendar-month-row">
							<div class="calendar-month-time">
								<time>
									<span class="m">Jan</span>
									<span class="d">03</span>
									<span class="y">2015</span>
								</time>
							</div><!-- .calendar-month-time -->
							<div class="calendar-month-title">
								<span class="block">2A Boys Outdoor Soccer</span>
							</div><!-- .calendar-month-title -->
							<div class="calendar-month-loc">
								<span class="block">Mount Pearl Sr. High</span>
							</div><!-- .calendar-month-loc -->
							<div class="calendar-month-button">
								<a href="#" class="button fill primary small">Register</a>
							</div><!-- .calendar-month-button -->
						</div><!-- .calendar-month-row -->

						<div class="calendar-month-row">
							<div class="calendar-month-time">
								<time>
									<span class="m">Jan</span>
									<span class="d">03</span>
									<span class="y">2015</span>
								</time>
							</div><!-- .calendar-month-time -->
							<div class="calendar-month-title">
								<span class="block">2A Boys Outdoor Soccer</span>
							</div><!-- .calendar-month-title -->
							<div class="calendar-month-loc">
								<span class="block">Mount Pearl Sr. High</span>
							</div><!-- .calendar-month-loc -->
							<div class="calendar-month-button">
								<a href="#" class="button fill primary small">Register</a>
							</div><!-- .calendar-month-button -->
						</div><!-- .calendar-month-row -->

						<div class="calendar-month-row">
							<div class="calendar-month-time">
								<time>
									<span class="m">Jan</span>
									<span class="d">03</span>
									<span class="y">2015</span>
								</time>
							</div><!-- .calendar-month-time -->
							<div class="calendar-month-title">
								<span class="block">2A Boys Outdoor Soccer</span>
							</div><!-- .calendar-month-title -->
							<div class="calendar-month-loc">
								<span class="block">Mount Pearl Sr. High</span>
							</div><!-- .calendar-month-loc -->
							<div class="calendar-month-button">
								<a href="#" class="button fill primary small">Register</a>
							</div><!-- .calendar-month-button -->
						</div><!-- .calendar-month-row -->

						<div class="calendar-month-row">
							<div class="calendar-month-time">
								<time>
									<span class="m">Jan</span>
									<span class="d">03</span>
									<span class="y">2015</span>
								</time>
							</div><!-- .calendar-month-time -->
							<div class="calendar-month-title">
								<span class="block">2A Boys Outdoor Soccer</span>
							</div><!-- .calendar-month-title -->
							<div class="calendar-month-loc">
								<span class="block">Mount Pearl Sr. High</span>
							</div><!-- .calendar-month-loc -->
							<div class="calendar-month-button">
								<a href="#" class="button fill primary small">Register</a>
							</div><!-- .calendar-month-button -->
						</div><!-- .calendar-month-row -->

						<div class="calendar-month-row">
							<div class="calendar-month-time">
								<time>
									<span class="m">Jan</span>
									<span class="d">03</span>
									<span class="y">2015</span>
								</time>
							</div><!-- .calendar-month-time -->
							<div class="calendar-month-title">
								<span class="block">2A Boys Outdoor Soccer</span>
							</div><!-- .calendar-month-title -->
							<div class="calendar-month-loc">
								<span class="block">Mount Pearl Sr. High</span>
							</div><!-- .calendar-month-loc -->
							<div class="calendar-month-button">
								<a href="#" class="button fill primary small">Register</a>
							</div><!-- .calendar-month-button -->
						</div><!-- .calendar-month-row -->

						<a href="#" class="tournament-list-more">
							<span>View All Qualifier Tournaments</span>
						</a>

					</div><!-- .tournament-list -->

					<div class="tournament-list">

						<h4>Qualifier Tournaments</h4>

						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
							Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. 
							Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, 
							nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.
						</p>

						<span class="tournament-list-title">St. John's</span>

						<div class="calendar-month-row">
							<div class="calendar-month-time">
								<time>
									<span class="m">Jan</span>
									<span class="d">03</span>
									<span class="y">2015</span>
								</time>
							</div><!-- .calendar-month-time -->
							<div class="calendar-month-title">
								<span class="block">2A Boys Outdoor Soccer</span>
							</div><!-- .calendar-month-title -->
							<div class="calendar-month-loc">
								<span class="block">Mount Pearl Sr. High</span>
							</div><!-- .calendar-month-loc -->
							<div class="calendar-month-button">
								<a href="#" class="button fill primary small">Register</a>
							</div><!-- .calendar-month-button -->
						</div><!-- .calendar-month-row -->

						<div class="calendar-month-row">
							<div class="calendar-month-time">
								<time>
									<span class="m">Jan</span>
									<span class="d">03</span>
									<span class="y">2015</span>
								</time>
							</div><!-- .calendar-month-time -->
							<div class="calendar-month-title">
								<span class="block">2A Boys Outdoor Soccer</span>
							</div><!-- .calendar-month-title -->
							<div class="calendar-month-loc">
								<span class="block">Mount Pearl Sr. High</span>
							</div><!-- .calendar-month-loc -->
							<div class="calendar-month-button">
								<a href="#" class="button fill primary small">Register</a>
							</div><!-- .calendar-month-button -->
						</div><!-- .calendar-month-row -->

						<div class="calendar-month-row">
							<div class="calendar-month-time">
								<time>
									<span class="m">Jan</span>
									<span class="d">03</span>
									<span class="y">2015</span>
								</time>
							</div><!-- .calendar-month-time -->
							<div class="calendar-month-title">
								<span class="block">2A Boys Outdoor Soccer</span>
							</div><!-- .calendar-month-title -->
							<div class="calendar-month-loc">
								<span class="block">Mount Pearl Sr. High</span>
							</div><!-- .calendar-month-loc -->
							<div class="calendar-month-button">
								<a href="#" class="button fill primary small">Register</a>
							</div><!-- .calendar-month-button -->
						</div><!-- .calendar-month-row -->

						<div class="calendar-month-row">
							<div class="calendar-month-time">
								<time>
									<span class="m">Jan</span>
									<span class="d">03</span>
									<span class="y">2015</span>
								</time>
							</div><!-- .calendar-month-time -->
							<div class="calendar-month-title">
								<span class="block">2A Boys Outdoor Soccer</span>
							</div><!-- .calendar-month-title -->
							<div class="calendar-month-loc">
								<span class="block">Mount Pearl Sr. High</span>
							</div><!-- .calendar-month-loc -->
							<div class="calendar-month-button">
								<a href="#" class="button fill primary small">Register</a>
							</div><!-- .calendar-month-button -->
						</div><!-- .calendar-month-row -->

						<div class="calendar-month-row">
							<div class="calendar-month-time">
								<time>
									<span class="m">Jan</span>
									<span class="d">03</span>
									<span class="y">2015</span>
								</time>
							</div><!-- .calendar-month-time -->
							<div class="calendar-month-title">
								<span class="block">2A Boys Outdoor Soccer</span>
							</div><!-- .calendar-month-title -->
							<div class="calendar-month-loc">
								<span class="block">Mount Pearl Sr. High</span>
							</div><!-- .calendar-month-loc -->
							<div class="calendar-month-button">
								<a href="#" class="button fill primary small">Register</a>
							</div><!-- .calendar-month-button -->
						</div><!-- .calendar-month-row -->

						<span class="tournament-list-title">Avalon</span>

						<div class="calendar-month-row">
							<div class="calendar-month-time">
								<time>
									<span class="m">Jan</span>
									<span class="d">03</span>
									<span class="y">2015</span>
								</time>
							</div><!-- .calendar-month-time -->
							<div class="calendar-month-title">
								<span class="block">2A Boys Outdoor Soccer</span>
							</div><!-- .calendar-month-title -->
							<div class="calendar-month-loc">
								<span class="block">Mount Pearl Sr. High</span>
							</div><!-- .calendar-month-loc -->
							<div class="calendar-month-button">
								<a href="#" class="button fill primary small">Register</a>
							</div><!-- .calendar-month-button -->
						</div><!-- .calendar-month-row -->

						<div class="calendar-month-row">
							<div class="calendar-month-time">
								<time>
									<span class="m">Jan</span>
									<span class="d">03</span>
									<span class="y">2015</span>
								</time>
							</div><!-- .calendar-month-time -->
							<div class="calendar-month-title">
								<span class="block">2A Boys Outdoor Soccer</span>
							</div><!-- .calendar-month-title -->
							<div class="calendar-month-loc">
								<span class="block">Mount Pearl Sr. High</span>
							</div><!-- .calendar-month-loc -->
							<div class="calendar-month-button">
								<a href="#" class="button fill primary small">Register</a>
							</div><!-- .calendar-month-button -->
						</div><!-- .calendar-month-row -->

						<div class="calendar-month-row">
							<div class="calendar-month-time">
								<time>
									<span class="m">Jan</span>
									<span class="d">03</span>
									<span class="y">2015</span>
								</time>
							</div><!-- .calendar-month-time -->
							<div class="calendar-month-title">
								<span class="block">2A Boys Outdoor Soccer</span>
							</div><!-- .calendar-month-title -->
							<div class="calendar-month-loc">
								<span class="block">Mount Pearl Sr. High</span>
							</div><!-- .calendar-month-loc -->
							<div class="calendar-month-button">
								<a href="#" class="button fill primary small">Register</a>
							</div><!-- .calendar-month-button -->
						</div><!-- .calendar-month-row -->

						<div class="calendar-month-row">
							<div class="calendar-month-time">
								<time>
									<span class="m">Jan</span>
									<span class="d">03</span>
									<span class="y">2015</span>
								</time>
							</div><!-- .calendar-month-time -->
							<div class="calendar-month-title">
								<span class="block">2A Boys Outdoor Soccer</span>
							</div><!-- .calendar-month-title -->
							<div class="calendar-month-loc">
								<span class="block">Mount Pearl Sr. High</span>
							</div><!-- .calendar-month-loc -->
							<div class="calendar-month-button">
								<a href="#" class="button fill primary small">Register</a>
							</div><!-- .calendar-month-button -->
						</div><!-- .calendar-month-row -->

						<div class="calendar-month-row">
							<div class="calendar-month-time">
								<time>
									<span class="m">Jan</span>
									<span class="d">03</span>
									<span class="y">2015</span>
								</time>
							</div><!-- .calendar-month-time -->
							<div class="calendar-month-title">
								<span class="block">2A Boys Outdoor Soccer</span>
							</div><!-- .calendar-month-title -->
							<div class="calendar-month-loc">
								<span class="block">Mount Pearl Sr. High</span>
							</div><!-- .calendar-month-loc -->
							<div class="calendar-month-button">
								<a href="#" class="button fill primary small">Register</a>
							</div><!-- .calendar-month-button -->
						</div><!-- .calendar-month-row -->

						<a href="#" class="tournament-list-more">
							<span>View All Qualifier Tournaments</span>
						</a>

					</div><!-- .tournament-list -->

					<h4>Provincial Tournaments</h4>

					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
						Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. 
						Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, 
						nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.
					</p>

					<div class="grid collapse-750">

						<div class="col col-2">
							<div class="item">
								
								<div class="block-with-time">

									<time datetime="2015-02-20">
										<span class="m-d">Feb 20</span>
										<span class="y">2015</span>
									</time>

									<div class="content">
										<a href="#" class="title">Provincial Table Tennis Championship</a>
										<span>Dorset Collegiate | Pelley's Island</span>
									</div><!-- .content -->

									<a href="#" class="button primary fill">More Info</a>

								</div><!-- .block-with-time -->

							</div><!-- .item -->
						</div><!-- .col -->

						<div class="col col-2">
							<div class="item">
								
								<div class="block-with-time">

									<time datetime="2015-02-20">
										<span class="m-d">Feb 20</span>
										<span class="y">2015</span>
									</time>

									<div class="content">
										<a href="#" class="title">Provincial Table Tennis Championship</a>
										<span>Dorset Collegiate | Pelley's Island</span>
									</div><!-- .content -->

									<a href="#" class="button primary fill">More Info</a>

								</div><!-- .block-with-time -->

							</div><!-- .item -->
						</div><!-- .col -->

						<div class="col col-2">
							<div class="item">
								
								<div class="block-with-time">

									<time datetime="2015-02-20">
										<span class="m-d">Feb 20</span>
										<span class="y">2015</span>
									</time>

									<div class="content">
										<a href="#" class="title">Provincial Table Tennis Championship</a>
										<span>Dorset Collegiate | Pelley's Island</span>
									</div><!-- .content -->

									<a href="#" class="button primary fill">More Info</a>

								</div><!-- .block-with-time -->

							</div><!-- .item -->
						</div><!-- .col -->

						<div class="col col-2">
							<div class="item">
								
								<div class="block-with-time">

									<time datetime="2015-02-20">
										<span class="m-d">Feb 20</span>
										<span class="y">2015</span>
									</time>

									<div class="content">
										<a href="#" class="title">Provincial Table Tennis Championship</a>
										<span>Dorset Collegiate | Pelley's Island</span>
									</div><!-- .content -->

									<a href="#" class="button primary fill">More Info</a>

								</div><!-- .block-with-time -->

							</div><!-- .item -->
						</div><!-- .col -->

					</div><!-- .grid -->

				</div><!-- .content -->

				<div class="sidebar">
					
					<?php include('inc/i-sidebar-calendar.php'); ?>

					<div class="sidebar-mod links-mod">
						<ul>
							<li><a href="#">Tournament Registration Procedures</a></li>
							<li><a href="#">Technical Standards - All Sports</a></li>
							<li><a href="#">Qualification Tournament Report Form</a></li>
							<li><a href="#">Provincial Tournament Report Form</a></li>
							<li><a href="#">Free Milk Order Form</a></li>
						</ul>
					</div><!-- .sidebar-mod -->

					<?php include('inc/i-sidebar-buttons.php'); ?>

				</div><!-- .sidebar -->

			</div><!-- .main-body -->


		</div><!-- .sw -->
	</section>

	<section class="d-bg primary-bg">
		<div class="sw">
			
			<?php include('inc/i-latest-tweet.php'); ?>

		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>