<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero-swiper swiper-wrapper">
	<div class="swiper"
		data-dots="true"
		data-arrows="false"
		data-fade="true"
		data-autoplay="true"
		data-autoplay-speed="5000"
		data-pause-on-hover="false"
		data-update-lazy-images="true">

		<div class="swipe-item">
			<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-soccer.jpg"></div>
		</div><!-- .swipe-item -->

		<div class="swipe-item">
			<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-soccer-i.jpg"></div>
		</div><!-- .swipe-item -->

	</div><!-- .swiper -->
</div><!-- .hero-swiper -->

<div class="body">

	<section class="excerpt-block">
		<div class="sw">
			
			<div>

				<div class="hgroup centered">
					<h1 class="hgroup-title">Mount Pearl Senior High</h1>
				</div><!-- .hgroup.centered -->

				<div class="school-image-logo">
					
					<div class="lazybg with-img">
						<img src="../assets/images/temp/school.jpg" alt="School">
					</div><!-- .lazybg.with-img -->
					<div class="lazybg with-img">
						<img src="../assets/images/temp/school-logo.jpg" alt="School Logo">
					</div><!-- .lazybg.with-img -->

				</div><!-- .school-image-logo -->

			</div>


		</div><!-- .sw -->
	</section>

	<section class="light-bg">
		<div class="sw">

			<div class="main-body">

				<div class="content">
					
					<div class="results-title-bar">
						<h2>2014-15 Sports Results</h2>

						<div class="selector with-arrow">
							<select>
								<option value="">Select Year</option>
								<option value="1">2013-14</option>								
								<option value="1">2014-15</option>
								<option value="1">2015-16</option>
							</select>
							<span class="value"></span>
						</div><!-- .selector -->
					</div><!-- .results-title-bar -->

					<h4>Official Sports</h4>

					<div class="results-container">
						<div class="results-header results-row">
							<span class="sport">&nbsp;</span>
							<span class="x2">Qualifiers</span>
							<span class="x2">Provincials</span>
							<span class="x4">Sportsmanship Banners</span>
							<span class="stars">Sports Star Results</span>
						</div><!-- .results-row -->

						<div class="results-subheader results-row">
							<span class="sport">&nbsp;</span>
							<span>Girls</span>
							<span>Boys</span>
							<span>Girls</span>
							<span>Boys</span>
							<span>GQ</span>
							<span>BQ</span>
							<span>GP</span>
							<span>BP</span>
							<span class="stars">&nbsp;</span>
						</div><!-- .results-row -->

						<div class="results-row">
							<span class="sport">Slo-Pitch</span>
							<span data-col="Qual. Girls">1st</span>
							<span data-col="Qual. Boys">&mdash;</span>
							<span data-col="Prov. Girls">3rd</span>
							<span data-col="Prov. Boys">&mdash;</span>
							<span data-col="GQ">
								<span class="badge">&nbsp;</span>
							</span>
							<span data-col="BQ">
								<span class="badge">&nbsp;</span>
							</span>
							<span data-col="GP">
								<span class="badge">&nbsp;</span>
							</span>
							<span data-col="BP">
								<span class="badge">&nbsp;</span>
							</span>
							<span class="stars">643</span>

						</div><!-- .results-row -->

						<div class="results-row">
							<span class="sport">Outdoor Soccer</span>
							<span data-col="Qual. Girls">1st</span>
							<span data-col="Qual. Boys">&mdash;</span>
							<span data-col="Prov. Girls">3rd</span>
							<span data-col="Prov. Boys">&mdash;</span>
							<span data-col="GQ">
								<span class="badge">&nbsp;</span>
							</span>
							<span data-col="BQ">
								<span class="badge">&nbsp;</span>
							</span>
							<span data-col="GP">
								<span class="badge">&nbsp;</span>
							</span>
							<span data-col="BP">
								<span class="badge">&nbsp;</span>
							</span>
							<span data-col="Sports Stars Results" class="stars">643</span>

						</div><!-- .results-row -->

					</div><!-- .results-container -->

					<h4>Demonstration Sports</h4>

					<div class="results-container">
						<div class="results-header results-row">
							<span class="sport">&nbsp;</span>
							<span class="x2">Qualifiers</span>
							<span class="x2">Provincials</span>
							<span class="x4">Sportsmanship Banners</span>
							<span class="stars">Sports Star Results</span>
						</div><!-- .results-row -->

						<div class="results-subheader results-row">
							<span class="sport">&nbsp;</span>
							<span>Girls</span>
							<span>Boys</span>
							<span>Girls</span>
							<span>Boys</span>
							<span>GQ</span>
							<span>BQ</span>
							<span>GP</span>
							<span>BP</span>
							<span class="stars">&nbsp;</span>
						</div><!-- .results-row -->

						<div class="results-row">
							<span class="sport">Golf</span>
							<span data-col="Qual. Girls">1st</span>
							<span data-col="Qual. Boys">&mdash;</span>
							<span data-col="Prov. Girls">3rd</span>
							<span data-col="Prov. Boys">&mdash;</span>
							<span data-col="GQ">
								<span class="badge">&nbsp;</span>
							</span>
							<span data-col="BQ">
								<span class="badge">&nbsp;</span>
							</span>
							<span data-col="GP">
								<span class="badge">&nbsp;</span>
							</span>
							<span data-col="BP">
								<span class="badge">&nbsp;</span>
							</span>
							<span data-col="Sports Stars Results" class="stars">643</span>

						</div><!-- .results-row -->

						<div class="results-row">
							<span class="sport">Touch Football</span>
							<span data-col="Qual. Girls">2nd</span>
							<span data-col="Qual. Boys">3rd</span>
							<span data-col="Prov. Girls">1st</span>
							<span data-col="Prov. Boys">4th</span>
							<span data-col="GQ">
								<span class="badge">&nbsp;</span>
							</span>
							<span data-col="BQ">
								<span class="badge">&nbsp;</span>
							</span>
							<span data-col="GP">
								<span class="badge">&nbsp;</span>
							</span>
							<span data-col="BP">
								<span class="badge">&nbsp;</span>
							</span>
							<span data-col="Sports Stars Results" class="stars">450</span>

						</div><!-- .results-row -->

					</div><!-- .results-container -->

					<div class="results-container">
						<div class="results-row">
							<span class="all"><strong>Sports Stars Total</strong></span>
							<span class="stars"><strong>2186</strong></span>
						</div>
						<div class="results-row">
							<span class="all"><strong>Sports Stars Level Reached</strong></span>
							<span class="stars">

								<span class="badge-block">
									<span class="badge gold"></span>
									<span class="block">10</span>
								</span>

								<span class="badge-block">
									<span class="badge silver"></span>
									<span class="block">8</span>
								</span>

								<span class="badge-block">
									<span class="badge bronze"></span>
									<span class="block">9</span>
								</span>

								<span class="badge-block">
									<span class="block badge-rep uc">Part.</span>
									<span class="block">11</span>
								</span>

							</span>
						</div>
					</div><!-- .results-container -->


					<div class="results-legend secondary-bg d-bg">
						<h5>Legend</h5>

						<ul>
							<li>GQ - Girls Qualifier</li>
							<li>GP - Girls Provincial</li>
							<li>BQ - Boys Qualifier</li>
							<li>BP - Boys Provincial</li>
							<li>PART. - Participation</li>
						</ul>
					</div>

				</div><!-- .content -->

				<div class="sidebar">
					
						<div class="sidebar-mod padded-mod">
							<h3>Contact</h3>

							<address>
								School Location <br>
								School Town, NL A1B 2C3
							</address>

							<br>

							<span class="block">P. 709.123.4567</span>
							<span class="block">f. 709.123.7891</span>

						</div><!-- .sidebar-mod -->

						<div class="sidebar-mod padded-mod">
							
							<div class="row">
								<span class="l">Region:</span>
								<span class="r">School Region</span>
							</div><!-- .row -->

							<div class="row">
								<span class="l">Classification:</span>
								<span class="r">School Classification</span>
							</div><!-- .row -->

							<div class="row">
								<span class="l">Membership Status:</span>
								<span class="r">Status</span>
							</div><!-- .row -->

						</div><!-- .sidebar-mod.padded-mod -->

						<div class="sidebar-mod padded-mod">
							<a href="#link" class="button secondary fill block">Photos</a>
						</div><!-- .padded-mod -->

						<div class="sidebar-mod links-mod">
							<ul>
								<li><a href="#">SSNL Calendar</a></li>
								<li><a href="#">Tournament Registration Procesudes</a></li>
								<li><a href="#">Technical Standards</a></li>
								<li><a href="#">Wildcard Application</a></li>
								<li><a href="#">Free Milk Order Form</a></li>
							</ul>
						</div><!-- .sidebar-mod -->

						<?php include('inc/i-sidebar-buttons.php'); ?>

				</div><!-- .sidebar -->

			</div><!-- .main-body -->


		</div><!-- .sw -->
	</section>

	<section class="d-bg primary-bg">
		<div class="sw">
			
			<?php include('inc/i-latest-tweet.php'); ?>

		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>