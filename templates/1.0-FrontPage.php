<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

<div class="home-hero lazybg" data-src="../assets/images/temp/hero/hero-home.jpg">
	
	<div class="home-hero-content">
		<h1 class="home-hero-title">Creating School Community Through Sport</h1>

		<p>
			Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
			Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.
		</p>
	</div><!-- .home-hero-content -->

</div><!-- .home-hero -->

<div class="body">

	<section class="nopad">
		<div class="grid home-logos nopad collapse-750">
			<div class="col col-2">
				<div class="item">

					<a href="#">
						<img src="../assets/images/ssnl-logo.svg" alt="school sports newfoundland and labrador logo">
						<span class="button fill primary">Visit Now</span>
					</a>
					
					
				</div><!-- .item -->
			</div><!-- .col -->
			<div class="col col-2">
				<div class="item">
					
					<a href="#">
						<img src="../assets/images/participation-nation-logo.svg" alt="participation nation logo">
						<span class="button fill primary">Visit Now</span>
					</a>

				</div><!-- .item -->
			</div><!-- .col -->
		</div>
	</section>

	<section class="d-bg primary-bg">
		<div class="sw">
			
			<?php include('inc/i-latest-tweet.php'); ?>

		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>