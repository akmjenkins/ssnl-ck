<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section class="excerpt-block">
		<div class="sw">
			
			<div>

				<div class="hgroup centered">
					<h1 class="hgroup-title">Sports</h1>
				</div><!-- .hgroup.centered -->

				<p class="excerpt">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
					Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar 
					tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
					Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien 
					nunc eget odio.
				</p>				
			</div>


		</div><!-- .sw -->
	</section>

	<section class="light-bg">
		<div class="sw">

			<div class="main-body">

				<div class="content">
					
					<div class="grid eqh sport-tiles">

						<div class="col-4 col sm-col-3 xs-col-1 sport-tile">
							<a class="item" href="#">
								<span class="sport-tile-title">Slopitch</span>
							</a><!-- .item -->
						</div><!-- .col -->

						<div class="col-4 col sm-col-3 xs-col-1 sport-tile">
							<a class="item" href="#">
								<span class="sport-tile-title">Outdoor Soccer</span>
							</a><!-- .item -->
						</div><!-- .col -->		

						<div class="col-4 col sm-col-3 xs-col-1 sport-tile">
							<a class="item" href="#">
								<span class="sport-tile-title">Cross-Country Running</span>
							</a><!-- .item -->
						</div><!-- .col -->

						<div class="col-4 col sm-col-3 xs-col-1 sport-tile">
							<a class="item" href="#">
								<span class="sport-tile-title">Indoor Soccer</span>
							</a><!-- .item -->
						</div><!-- .col -->

						<div class="col-4 col sm-col-3 xs-col-1 sport-tile">
							<a class="item" href="#">
								<span class="sport-tile-title">Volleyball</span>
							</a><!-- .item -->
						</div><!-- .col -->

						<div class="col-4 col sm-col-3 xs-col-1 sport-tile">
							<a class="item" href="#">
								<span class="sport-tile-title">Table Tennis</span>
							</a><!-- .item -->
						</div><!-- .col -->

						<div class="col-4 col sm-col-3 xs-col-1 sport-tile">
							<a class="item" href="#">
								<span class="sport-tile-title">Cross-Country Skiing</span>
							</a><!-- .item -->
						</div><!-- .col -->

						<div class="col-4 col sm-col-3 xs-col-1 sport-tile">
							<a class="item" href="#">
								<span class="sport-tile-title">Wrestling</span>
							</a><!-- .item -->
						</div><!-- .col -->

						<div class="col-4 col sm-col-3 xs-col-1 sport-tile">
							<a class="item" href="#">
								<span class="sport-tile-title">Basketball</span>
							</a><!-- .item -->
						</div><!-- .col -->

						<div class="col-4 col sm-col-3 xs-col-1 sport-tile">
							<a class="item" href="#">
								<span class="sport-tile-title">Ball Hockey</span>
							</a><!-- .item -->
						</div><!-- .col -->

						<div class="col-4 col sm-col-3 xs-col-1 sport-tile">
							<a class="item" href="#">
								<span class="sport-tile-title">Badminton</span>
							</a><!-- .item -->
						</div><!-- .col -->

						<div class="col-4 col sm-col-3 xs-col-1 sport-tile">
							<a class="item" href="#">
								<span class="sport-tile-title">Track &amp; Field</span>
							</a><!-- .item -->
						</div><!-- .col -->				
					</div><!-- .grid -->

					<div class="d-bg secondary-bg pad-20">
						<div class="hgroup centered section-header">
							<h3 class="hgroup-title">Demonstration Sports</h3>
						</div><!-- .hgroup -->

						<div class="grid eqh sport-tiles">

							<div class="col col-3 xs-col-1 sport-tile">
								<a class="item" href="#">
									<span class="sport-tile-title">Golf</span>
								</a><!-- .item -->
							</div><!-- .col -->

							<div class="col col-3 xs-col-1 sport-tile">
								<a class="item" href="#">
									<span class="sport-tile-title">Touch Football</span>
								</a><!-- .item -->
							</div><!-- .col -->

							<div class="col col-3 xs-col-1 sport-tile">
								<a class="item" href="#">
									<span class="sport-tile-title">Ice Hockey</span>
								</a><!-- .item -->
							</div><!-- .col -->

						</div><!-- .grid -->
					</div><!-- .d-bg -->


				</div><!-- .content -->

				<div class="sidebar">
					
					<?php include('inc/i-sidebar-calendar.php'); ?>

					<div class="sidebar-mod links-mod">
						<ul>
							<li><a href="#">Tournament Registration Procedures</a></li>
							<li><a href="#">Technical Standards - All Sports</a></li>
							<li><a href="#">Qualification Tournament Report Form</a></li>
							<li><a href="#">Provincial Tournament Report Form</a></li>
							<li><a href="#">Free Milk Order Form</a></li>
						</ul>
					</div><!-- .sidebar-mod -->

					<?php include('inc/i-sidebar-buttons.php'); ?>

				</div><!-- .sidebar -->

			</div><!-- .main-body -->

		</div><!-- .sw -->
	</section>

	<section class="d-bg primary-bg">
		<div class="sw">
			
			<?php include('inc/i-latest-tweet.php'); ?>

		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>