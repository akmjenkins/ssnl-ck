<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero-swiper swiper-wrapper">
	<div class="swiper"
		data-dots="true"
		data-arrows="false"
		data-fade="true"
		data-autoplay="true"
		data-autoplay-speed="5000"
		data-pause-on-hover="false"
		data-update-lazy-images="true">

		<div class="swipe-item">
			<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-soccer.jpg"></div>
		</div><!-- .swipe-item -->

		<div class="swipe-item">
			<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-soccer-i.jpg"></div>
		</div><!-- .swipe-item -->

	</div><!-- .swiper -->
</div><!-- .hero-swiper -->

<div class="body">

	<section class="excerpt-block">
		<div class="sw">
			
			<div>

				<div class="hgroup centered">
					<h1 class="hgroup-title">3A Girls Soccer</h1>
				</div><!-- .hgroup.centered -->

				<span class="custom-f-abs ssnl-soccer title-ico">Soccer</span>

			</div>


		</div><!-- .sw -->
	</section>

	<section class="light-bg">
		<div class="sw">

			<div class="main-body">

				<div class="content">
					
					<div class="hgroup centered">
						<h2 class="hgroup-title">Host School Name</h2>
					</div><!-- .hgroup-centered -->

					<div class="school-image-logo">
						
						<div class="lazybg with-img">
							<img src="../assets/images/temp/school.jpg" alt="School">
						</div><!-- .lazybg.with-img -->
						<div class="lazybg with-img">
							<img src="../assets/images/temp/school-logo.jpg" alt="School Logo">
						</div><!-- .lazybg.with-img -->

					</div><!-- .school-image-logo -->

					<br>

					<div class="center">
						
						<h5>Qualified Schools</h5>

						<ul class="two-col">
							<li>School One</li>
							<li>School Two</li>
							<li>School Three</li>
							<li>School Four</li>
							<li>School Five</li>
							<li>School Six</li>
						</ul>

					</div><!-- .center -->

					<hr>

					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
						Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar 
						tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
						Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus 
						sapien nunc eget odio.						
					</p>

					<hr>


					<h5>Division A</h5>

					<div class="standings-container">
						<div class="standings-header standings-row">
							<span class="standings-row-team">Team</span>
							<span class="standings-row-roster">&nbsp;</span>
							<span>W</span>
							<span>L</span>
							<span>T</span>
							<span>PTS</span>
						</div><!-- .standings-header -->

						<div class="standings-row">

							<span class="standings-row-team">
								<div class="selector light with-team-logo with-arrow">
									<span class="team-logo">&nbsp;</span>
									<select>
										<option value="">Select a School</option>
										<option value="1" data-logo="http://fillmurray.com/g/500/500">Team One</option>
										<option value="1" data-logo="http://fillmurray.com/g/400/400">Team Two</option>
										<option value="1" data-logo="http://fillmurray.com/g/300/300">Team Three</option>
										<option value="1" data-logo="http://fillmurray.com/g/200/200">Team Four</option>
									</select>
									<span class="value"></span>
								</div><!-- .selector -->
							</span><!-- .standings-row-team -->
							<span class="standings-row-roster">
								<a href="#" class="button fill primary vsmall">Roster</a>
							</span>
							<span data-col="W">2</span>
							<span data-col="L">0</span>
							<span data-col="T">1</span>
							<span data-col="PTS">5</span>

						</div><!-- .standings-row -->

						<div class="standings-row">
							<span class="standings-row-team">
								<div class="selector light with-team-logo with-arrow">
									<span class="team-logo">&nbsp;</span>
									<select>
										<option value="">Select a School</option>
										<option value="1" data-logo="http://fillmurray.com/g/500/500">Team One</option>
										<option value="1" data-logo="http://fillmurray.com/g/400/400">Team Two</option>
										<option value="1" data-logo="http://fillmurray.com/g/300/300">Team Three</option>
										<option value="1" data-logo="http://fillmurray.com/g/200/200">Team Four</option>
									</select>
									<span class="value"></span>
								</div><!-- .selector -->
							</span><!-- .standings-row-team -->
							<span class="standings-row-roster">
								<a href="#" class="button fill primary vsmall">Roster</a>
							</span>
							<span data-col="W">2</span>
							<span data-col="L">1</span>
							<span data-col="T">0</span>
							<span data-col="PTS">4</span>
						</div><!-- .standings-row -->

					</div><!-- .standings-container -->

					<button class="form-action form-action-add">Add Team</button>

				</div><!-- .content -->

				<div class="sidebar">

					<!-- sidebar content isn't useful on this page below 950px, and actually just gets in the way -->
					<div class="no-950">
					
						<?php include('inc/i-sidebar-calendar.php'); ?>

						<div class="sidebar-mod padded-mod">
							<a href="#link" class="button secondary fill block">Photos</a>
						</div><!-- .padded-mod -->

						<div class="sidebar-mod links-mod">
							<ul>
								<li><a href="#">Tournament Registration Procedures</a></li>
								<li><a href="#">Technical Standards - All Sports</a></li>
								<li><a href="#">Qualification Tournament Report Form</a></li>
								<li><a href="#">Provincial Tournament Report Form</a></li>
								<li><a href="#">Free Milk Order Form</a></li>
							</ul>
						</div><!-- .sidebar-mod -->

						<?php include('inc/i-sidebar-buttons.php'); ?>

						<a href="#" class="sidebar-mod live-feed-mod">
							<div class="lazybg" data-src="../assets/images/temp/live-feed.jpg"></div>
							<span>View Live Feed</span>
						</a><!-- .live-feed-mod -->

					</div><!-- .no-950 -->

				</div><!-- .sidebar -->

			</div><!-- .main-body -->


		</div><!-- .sw -->
	</section>

	<section class="md-bg">
		<div class="sw">
			
			<h5>Schedule &amp; Results</h5>

			<div class="schedule-results-container">

				<div class="schedule-results-header schedule-results-row">
					<span>Game</span>
					<span class="time">Time</span>
					<span class="location">Location</span>
					<span class="with-selector">Team One</span>
					<span>Score</span>
					<span>VS</span>
					<span>Score</span>
					<span class="with-selector">Team Two</span>
				</div><!-- .schedule-results-row -->

				<div class="schedule-results-row">
					<span data-col="Game">1</span>
					<span data-col="Time" class="time">9:00AM</span>
					<span data-col="Location" class="location">Field One</span>
					<span class="with-selector">
						<div class="selector light with-arrow">
							<select>
								<option value="">School Name</option>
								<option value="1">School One</option>
								<option value="1">School Two</option>
								<option value="1">School Three</option>
								<option value="1">School Four</option>
							</select>
							<span class="value"></span>
						</div><!-- .selector -->
					</span>
					<span>4</span>
					<span>VS</span>
					<span>1</span>
					<span class="with-selector">
						<div class="selector light with-arrow">
							<select>
								<option value="">School Name</option>
								<option value="1">School One</option>
								<option value="1">School Two</option>
								<option value="1">School Three</option>
								<option value="1">School Four</option>
							</select>
							<span class="value"></span>
						</div><!-- .selector -->
					</span>
				</div><!-- .schedule-results-row -->

				<div class="schedule-results-row">
					<span data-col="Game">2</span>
					<span data-col="Time" class="time">10:30AM</span>
					<span data-col="Location" class="location">Field Two</span>
					<span class="with-selector">
						<div class="selector light with-arrow">
							<select>
								<option value="">School Name</option>
								<option value="1">School One</option>
								<option value="1">School Two</option>
								<option value="1">School Three</option>
								<option value="1">School Four</option>
							</select>
							<span class="value"></span>
						</div><!-- .selector -->
					</span>
					<span>4</span>
					<span>VS</span>
					<span>1</span>
					<span class="with-selector">
						<div class="selector light with-arrow">
							<select>
								<option value="">School Name</option>
								<option value="1">School One</option>
								<option value="1">School Two</option>
								<option value="1">School Three</option>
								<option value="1">School Four</option>
							</select>
							<span class="value"></span>
						</div><!-- .selector -->
					</span>
				</div><!-- .schedule-results-row -->

				<div class="schedule-results-row">
					<span data-col="Game">3</span>
					<span data-col="Time" class="time">12:00PM</span>
					<span data-col="Location" class="location">Field Three</span>
					<span class="with-selector">
						<div class="selector light with-arrow">
							<select>
								<option value="">School Name</option>
								<option value="1">School One</option>
								<option value="1">School Two</option>
								<option value="1">School Three</option>
								<option value="1">School Four</option>
							</select>
							<span class="value"></span>
						</div><!-- .selector -->
					</span>
					<span>4</span>
					<span>VS</span>
					<span>1</span>
					<span class="with-selector">
						<div class="selector light with-arrow">
							<select>
								<option value="">School Name</option>
								<option value="1">School One</option>
								<option value="1">School Two</option>
								<option value="1">School Three</option>
								<option value="1">School Four</option>
							</select>
							<span class="value"></span>
						</div><!-- .selector -->
					</span>
				</div><!-- .schedule-results-row -->

			</div><!-- .schedule-results-container -->

			<button class="form-action form-action-add">Add Team</button>

			<hr>

			<div class="grid collapse-950">
				<div class="col col-2-5">
					<div class="item">
						
						<h5>Team Results</h5>

						<div class="flex-result-selector">
							<strong class="block uc label">Champions</strong>
							<div class="selector light with-team-logo with-arrow">
								<span class="team-logo">&nbsp;</span>
								<select>
									<option value="">Select a School</option>
									<option value="1" data-logo="http://fillmurray.com/g/500/500">Team One</option>
									<option value="1" data-logo="http://fillmurray.com/g/400/400">Team Two</option>
									<option value="1" data-logo="http://fillmurray.com/g/300/300">Team Three</option>
									<option value="1" data-logo="http://fillmurray.com/g/200/200">Team Four</option>
								</select>
								<span class="value"></span>
							</div><!-- .selector -->
						</div><!-- .flex-result-selector -->

						<div class="flex-result-selector">
							<strong class="block uc label">Runner-Up</strong>
							<div class="selector light with-team-logo with-arrow">
								<span class="team-logo">&nbsp;</span>
								<select>
									<option value="">Select a School</option>
									<option value="1" data-logo="http://fillmurray.com/g/500/500">Team One</option>
									<option value="1" data-logo="http://fillmurray.com/g/400/400">Team Two</option>
									<option value="1" data-logo="http://fillmurray.com/g/300/300">Team Three</option>
									<option value="1" data-logo="http://fillmurray.com/g/200/200">Team Four</option>
								</select>
								<span class="value"></span>
							</div><!-- .selector -->
						</div><!-- .flex-result-selector -->

						<div class="flex-result-selector">
							<strong class="block uc label">3rd Place</strong>
							<div class="selector light with-team-logo with-arrow">
								<span class="team-logo">&nbsp;</span>
								<select>
									<option value="">Select a School</option>
									<option value="1" data-logo="http://fillmurray.com/g/500/500">Team One</option>
									<option value="1" data-logo="http://fillmurray.com/g/400/400">Team Two</option>
									<option value="1" data-logo="http://fillmurray.com/g/300/300">Team Three</option>
									<option value="1" data-logo="http://fillmurray.com/g/200/200">Team Four</option>
								</select>
								<span class="value"></span>
							</div><!-- .selector -->
						</div><!-- .flex-result-selector -->

						<div class="flex-result-selector">
							<strong class="block uc label">4th Place</strong>
							<div class="selector light with-team-logo with-arrow">
								<span class="team-logo">&nbsp;</span>
								<select>
									<option value="">Select a School</option>
									<option value="1" data-logo="http://fillmurray.com/g/500/500">Team One</option>
									<option value="1" data-logo="http://fillmurray.com/g/400/400">Team Two</option>
									<option value="1" data-logo="http://fillmurray.com/g/300/300">Team Three</option>
									<option value="1" data-logo="http://fillmurray.com/g/200/200">Team Four</option>
								</select>
								<span class="value"></span>
							</div><!-- .selector -->
						</div><!-- .flex-result-selector -->

					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3-5">
					<div class="item">

						<h5>Individual Sportsmanship Winners</h5>

						<div class="grid pad10 body-form collapse-599 sportsmanship-grid">

							<div class="col col-2">
								<div class="item">
									<div class="selector light with-team-logo with-arrow">
										<span class="team-logo">&nbsp;</span>
										<select>
											<option value="">Select a School</option>
											<option value="1" data-logo="http://fillmurray.com/g/500/500">Team One</option>
											<option value="1" data-logo="http://fillmurray.com/g/400/400">Team Two</option>
											<option value="1" data-logo="http://fillmurray.com/g/300/300">Team Three</option>
											<option value="1" data-logo="http://fillmurray.com/g/200/200">Team Four</option>
										</select>
										<span class="value"></span>
									</div><!-- .selector -->
								</div><!-- .item -->
							</div><!-- .col -->
							<div class="col col-2">
								<div class="item">
									<input type="text" placeholder="Player Name">
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col col-2">
								<div class="item">
									<div class="selector light with-team-logo with-arrow">
										<span class="team-logo">&nbsp;</span>
										<select>
											<option value="">Select a School</option>
											<option value="1" data-logo="http://fillmurray.com/g/500/500">Team One</option>
											<option value="1" data-logo="http://fillmurray.com/g/400/400">Team Two</option>
											<option value="1" data-logo="http://fillmurray.com/g/300/300">Team Three</option>
											<option value="1" data-logo="http://fillmurray.com/g/200/200">Team Four</option>
										</select>
										<span class="value"></span>
									</div><!-- .selector -->
								</div><!-- .item -->
							</div><!-- .col -->
							<div class="col col-2">
								<div class="item">
									<input type="text" placeholder="Player Name">
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col col-2">
								<div class="item">
									<div class="selector light with-team-logo with-arrow">
										<span class="team-logo">&nbsp;</span>
										<select>
											<option value="">Select a School</option>
											<option value="1" data-logo="http://fillmurray.com/g/500/500">Team One</option>
											<option value="1" data-logo="http://fillmurray.com/g/400/400">Team Two</option>
											<option value="1" data-logo="http://fillmurray.com/g/300/300">Team Three</option>
											<option value="1" data-logo="http://fillmurray.com/g/200/200">Team Four</option>
										</select>
										<span class="value"></span>
									</div><!-- .selector -->
								</div><!-- .item -->
							</div><!-- .col -->
							<div class="col col-2">
								<div class="item">
									<input type="text" placeholder="Player Name">
								</div><!-- .item -->
							</div><!-- .col -->

							<div class="col col-2">
								<div class="item">
									<div class="selector light with-team-logo with-arrow">
										<span class="team-logo">&nbsp;</span>
										<select>
											<option value="">Select a School</option>
											<option value="1" data-logo="http://fillmurray.com/g/500/500">Team One</option>
											<option value="1" data-logo="http://fillmurray.com/g/400/400">Team Two</option>
											<option value="1" data-logo="http://fillmurray.com/g/300/300">Team Three</option>
											<option value="1" data-logo="http://fillmurray.com/g/200/200">Team Four</option>
										</select>
										<span class="value"></span>
									</div><!-- .selector -->
								</div><!-- .item -->
							</div><!-- .col -->
							<div class="col col-2">
								<div class="item">
									<input type="text" placeholder="Player Name">
								</div><!-- .item -->
							</div><!-- .col -->

						</div><!-- .grid -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-2-5">
					<div class="item">

						<h5>Team Sportsmanship Winner</h5>

						<div class="selector light with-team-logo with-arrow">
							<span class="team-logo">&nbsp;</span>
							<select>
								<option value="">Select a School</option>
								<option value="1" data-logo="http://fillmurray.com/g/500/500">Team One</option>
								<option value="1" data-logo="http://fillmurray.com/g/400/400">Team Two</option>
								<option value="1" data-logo="http://fillmurray.com/g/300/300">Team Three</option>
								<option value="1" data-logo="http://fillmurray.com/g/200/200">Team Four</option>
							</select>
							<span class="value"></span>
						</div><!-- .selector -->

					</div><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->

		</div><!-- .sw -->
	</section><!-- .md-bg -->

	<section class="d-bg primary-bg">
		<div class="sw">
			
			<?php include('inc/i-latest-tweet.php'); ?>

		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>