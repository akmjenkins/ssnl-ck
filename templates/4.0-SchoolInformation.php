<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section class="excerpt-block">
		<div class="sw">
			
			<div>

				<div class="hgroup centered">
					<h1 class="hgroup-title">SSNL Membership Registration</h1>
				</div><!-- .hgroup.centered -->

				<p class="excerpt">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
					Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar 
					tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
					Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien 
					nunc eget odio.
				</p>				
			</div>


		</div><!-- .sw -->
	</section>

	<section class="light-bg">
		<div class="sw">


			<form action="/" novalidate class="body-form full">

				<div class="registration-form">
				
					<div class="fieldset">

						<div class="hgroup section-header">
							<h3 class="hgroup-title">School Information</h3>
						</div><!-- .hgroup -->

						<div class="progress-bar">
							<span class="selected">School Information</span>
							<span>Representative Information</span>
							<span>Coach &amp; Sports</span>
							<span>Payment</span>
						</div><!-- .progress-bar -->
						
						<fieldset>
							<legend>I Am Registering For</legend>

							<div class="center pad-20">

								<label class="radio">
									<input type="radio" name="program_type" value="tournament">
									<span>High School Tournament Program</span>
								</label>

								<label class="radio">
									<input type="radio" name="program_type" value="participation_nation">
									<span>Participation Nation</span>
								</label>

							</div><!-- .center -->

						</fieldset>

						<fieldset>
							<legend>General Information</legend>

							<div class="grid pad10 collapse-599">

								<div class="col col-2">
									<div class="item">
										<div class="selector with-arrow light">
											<select name="school">
												<option value="">Select Your School</option>

												<!-- trim long school names to something that can fit in the selector box -->
												<option value="1">School 1</option>
												<option value="2">School 2</option>
												<option value="3">School 3</option>
												<option value="4">School 4</option>
											</select>
											<span class="value"></span>
										</div><!-- .selector -->
									</div><!-- .item -->
								</div><!-- .col -->

								<div class="col col-2">
									<div class="item">
										<input type="tel" name="population" placeholder="School Population: e.g. 450">
									</div><!-- .item -->
								</div><!-- .col -->

								<div class="col col-1">
									<div class="item">
										<div class="selector with-arrow light">
											<select name="school">
												<option value="">Region</option>

												<!-- trim long school names to something that can fit in the selector box -->
												<option value="1">School 1</option>
												<option value="2">School 2</option>
												<option value="3">School 3</option>
												<option value="4">School 4</option>
											</select>
											<span class="value"></span>
										</div><!-- .selector -->
									</div><!-- .item -->
								</div><!-- .col -->

								<div class="col col-2">
									<div class="item">
										<input type="text" name="address" placeholder="Address">
									</div><!-- .item -->
								</div><!-- .col -->

								<div class="col col-2">
									<div class="item">
										<input type="text" name="community" placeholder="Community">
									</div><!-- .item -->
								</div><!-- .col -->

								<div class="col col-2">
									<div class="item">
										<input type="tel" name="phone" placeholder="Phone">
									</div><!-- .item -->
								</div><!-- .col -->

								<div class="col col-2">
									<div class="item">
										<input type="tel" name="fax" placeholder="Fax">
									</div><!-- .item -->
								</div><!-- .col -->

								<div class="col col-1">
									<div class="item">
										<input type="text" name="website" placeholder="Website">
									</div><!-- .item -->
								</div><!-- .col -->

							</div><!-- .grid -->
						</fieldset>

						<fieldset>

							<legend>School Team Information</legend>

							<input type="text" name="team_name" placeholder="School Team Name (eg. Falcons, Hawks, Lynx...)">

							<div class="grid eqh collapse-599">

								<div class="col col-2">
									<div class="item">

										<span class="field-title">Team Logo</span>

										<label class="file">
											<input type="file" name="logo">
											<span class="note">Uploaded files must be in .jpg, .png or .gif format and cannot exceed 70kb in size</span>
											<span class="button primary fill">Upload Logo</span>
										</label>

									</div><!-- .item -->
								</div><!-- .col -->

								<div class="col col-2">
									<div class="item">

										<span class="field-title">Team Colours</span>
										
										<div class="selector with-arrow">
											<select name="primary_color">
												<option value="">Primary Color</option>
											</select>
											<span class="value"></span>
										</div><!-- .selector -->

										<div class="selector with-arrow">
											<select name="primary_color">
												<option value="">Secondary Color</option>
											</select>
											<span class="value"></span>
										</div><!-- .selector -->

									</div><!-- .item -->
								</div><!-- .col -->

							</div><!-- .grid -->
						</fieldset>

						<div class="form-controls">
							<button type="button" class="button fill primary next">Next</button>
						</div><!-- .form-controls -->

					</div><!-- .fieldset -->
				
				</div><!-- .registration-form -->
	
			</form>
			

		</div><!-- .sw -->
	</section><!-- .light-bg -->

	<section class="d-bg primary-bg">
		<div class="sw">
			
			<?php include('inc/i-latest-tweet.php'); ?>

		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>