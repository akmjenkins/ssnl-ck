<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section class="excerpt-block">
		<div class="sw">
			
			<div>

				<div class="hgroup centered">
					<h1 class="hgroup-title">Tournament Registration</h1>
				</div><!-- .hgroup.centered -->

				<p class="excerpt">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
					Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar 
					tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
					Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien 
					nunc eget odio.
				</p>				
			</div>


		</div><!-- .sw -->
	</section>

	<section class="light-bg">
		<div class="sw">


			<form action="/" novalidate class="body-form full">

				<div class="registration-form">

					<div class="fieldset">

						<div class="selector with-arrow">
							<select name="" id="">
								<option value="">Select Tournament Type</option>
								<option value="1">Zone Tournament</option>
								<option value="1">Qualifier Tournament</option>
								<option value="1">Provincial Tournament</option>
							</select>
							<span class="value">&nbsp;</span>
						</div><!-- .selector -->

						<fieldset>
							<div class="pad-20 center">
								<p>
									Has your school qualified for a provincial tournament by winning your regions qualification tournament, 
									receiving a wild card bid, or by receiving a bye?
								</p>

								<br>

								<label class="radio">
									<input type="radio" name="qualified" value="yes">
									<span>Yes</span>
								</label>

								<label class="radio">
									<input type="radio" name="qualified" value="no">
									<span>No</span>
								</label>

							</div><!-- .pad-20 -->
						</fieldset>

						<div class="center">
							<div class="selector with-arrow inline light">
								<select>
									<option value="">Select A Sport</option>
									<option value="1" data-tag="Slopitch">Slopitch</option>
									<option value="1" data-tag="Outdoor Soccer">Outdoor Soccer</option>
									<option value="1" data-tag="Cross-Country Running">Cross-Country Running</option>
									<option value="1" data-tag="Indoor Soccer">Indoor Soccer</option>
									<option value="1" data-tag="Volleyball">Volleyball</option>
									<option value="1" data-tag="Table Tennis">Table Tennis</option>
									<option value="1" data-tag="Cross-Country Skiing">Cross-Country Skiing</option>
									<option value="1" data-tag="Wrestling">Wrestling</option>
									<option value="1" data-tag="Basketball">Basketball</option>
								</select>
								<span class="value"></span>
							</div><!-- .selector -->
						</div>

						<br>
						<br>

						<div class="hgroup">
							<h3 class="hgroup-title">Track &amp; Field</h3>
						</div><!-- .hgroup -->

						<fieldset>
							<legend>General Information</legend>

							<div class="grid pad10 collapse-599">

								<div class="col col-2">
									<div class="item">
										<div class="selector with-arrow light">
											<select name="school">
												<option value="">Select Your School</option>

												<!-- trim long school names to something that can fit in the selector box -->
												<option value="1" data-tag="Trimmed School Name">School 1</option>
												<option value="2" data-tag="Trimmed School Name">School 2</option>
												<option value="3" data-tag="Trimmed School Name">School 3</option>
												<option value="4" data-tag="Trimmed School Name">School 4</option>
											</select>
											<span class="value"></span>
										</div><!-- .selector -->
									</div><!-- .item -->
								</div><!-- .col -->
								<div class="col col-2">
									<div class="item">
										<input type="tel" name="fax" placeholder="Phone">
									</div><!-- .item -->
								</div><!-- .col -->
								<div class="col col-1">
									<div class="item">
										<input type="tel" name="fax" placeholder="Fax">
									</div><!-- .item -->
								</div><!-- .col -->
							</div><!-- .grid -->
						</fieldset>

						<fieldset>
							<legend>Teacher Sponsor/Coach</legend>

							<div class="fieldset-box">
								<div class="fieldset-box-head">
									<span>Select Sport</span>
								</div><!-- .fieldset-box-head -->
								<div class="fieldset-box-content cf">
									
									<div class="grid pad10 collapse-599">
										<div class="col col-1">
											<div class="item">
												<span class="field-title">Teacher</span>
											</div><!-- .item -->
										</div><!-- .col -->
										<div class="col col-1">
											<div class="item">
												<div class="selector light with-arrow">
													<select name="teacher_sponsor[]">
														<option value="">Teacher Sponsor</option>
														<option value="1">Teacher Option One</option>
														<option value="2">Teacher Option Two</option>
														<option value="3">Teacher Option Three</option>
													</select>
													<span class="value"></span>
												</div><!-- .selector -->
											</div><!-- .item -->
										</div><!-- .col -->
										<div class="col col-1">
											<div class="item">
												<input type="email" name="teacher_sponsor_email[]" placeholder="Teacher Sponsor's E-mail">
											</div><!-- .item -->
										</div><!-- .col -->
									</div><!-- .grid -->

									<br>

									<div class="grid pad10 collapse-599">
										<div class="col col-1">
											<div class="item">
												<span class="field-title">Coach</span>
											</div><!-- .item -->
										</div><!-- .col -->
										<div class="col col-1">
											<div class="item">
												<div class="selector light with-arrow">
													<select name="coach[]">
														<option value="">Coach</option>
														<option value="1">Coach Option One</option>
														<option value="2">Coach Option Two</option>
														<option value="3">Coach Option Three</option>
													</select>
													<span class="value"></span>
												</div><!-- .selector -->
											</div><!-- .item -->
										</div><!-- .col -->
										<div class="col col-1">
											<div class="item">
												<input type="email" name="coach_email[]" placeholder="Coach's E-mail">
											</div><!-- .item -->
										</div><!-- .col -->
										<div class="col col-1">
											<div class="item">
												<input type="email" name="coach_email[]" placeholder="Coach's E-mail">
											</div><!-- .item -->
										</div><!-- .col -->
										<div class="col col-1">
											<span class="form-action form-action-add">Add Coach</span>
										</div>
									</div><!-- .grid -->

									<button type="button" class="f-right button fill primary">Done</button>

								</div><!-- .fieldset-box-content -->
							</div><!-- .fieldset-box -->
						</fieldset>

						<fieldset>
							<legend>Add Athlete</legend>

							<div class="fieldset-box">
								<div class="fieldset-box-head">
									<span>Athlete Information</span>
								</div><!-- .fieldset-box-head -->
								<div class="fieldset-box-content cf">
									
									<div class="grid pad10 collapse-599">
										<div class="col col-2-3">
											<div class="item">
												<input type="text" name="player_name" placeholder="Player Name">
											</div><!-- .item -->
										</div><!-- .col -->
										<div class="col col-1-3">
											<div class="item">
												<input type="text" name="grade" placeholder="Grade">
											</div><!-- .item -->
										</div><!-- .col -->
										<div class="col col-1-5">
											<div class="item">
												<input type="text" name="age" placeholder="Age">
											</div>
										</div>
										<div class="col col-1-5">
											<div class="item">
												<input type="text" name="jersey" placeholder="Jersey #">
											</div>
										</div>
										<div class="col col-3-5">
											<div class="item">
												<input type="text" name="banquet" placeholder="Banquet">
											</div>
										</div>
										<div class="col col-1">
											<div class="item">
												<textarea name="allergies" placeholder="Allergies" cols="30" rows="10"></textarea>
											</div><!-- .item -->
										</div><!-- .col -->
									</div><!-- .grid -->

									<span class="form-action form-action-add">Add Athlete</span>									

									<button type="button" class="f-right button fill primary">Done</button>

								</div><!-- .fieldset-box-content -->
							</div><!-- .fieldset-box -->
						</fieldset>

					</div><!-- .fieldset -->
				
				</div><!-- .registration-form -->
	
			</form>
			

		</div><!-- .sw -->
	</section><!-- .light-bg -->

	<section class="d-bg primary-bg">
		<div class="sw">
			
			<?php include('inc/i-latest-tweet.php'); ?>

		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>