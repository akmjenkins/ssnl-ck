<div class="latest-tweet">
	
	<span class="latest-tweet-ico">Latest Tweet</span>

	<div class="latest-tweet-content">
		Vivamus vel hendrerit mi. Aenean suscipit malesuada mi, in tristique mi lobortis quis. Duis sodales auctor mattis. Morbi dolor nunc posuere.
	</div><!-- .tweet-content -->

	<div class="latest-tweet-button">
		<a href="#" class="button tertiary fill">Follow Us</a>
	</div><!-- .tweet-button -->

</div><!-- .latest-tweet -->