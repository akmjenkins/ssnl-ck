			<footer>
			
				<div class="sw">
					<div class="footer-row">
					
						<div class="footer-contact">
							
							<span class="footer-contact-title">Contact</span>

							<address>
								1296A Kenmount Road <br>
								Box 8700 St. John's, NL <br>
								A1B 4J6
							</address>

							<a href="#" class="button fill primary">Staff Directory</a>

						</div><!-- .footer-contact -->
						
						<div class="footer-nav">
							
							<ul>
								<li><a href="#">About</a></li>
								<li><a href="#">Schools</a></li>
								<li><a href="#">Sports</a></li>
								<li><a href="#">Resources &amp; Forms</a></li>
								<li><a href="#">Sports Stars</a></li>
								<li><a href="#">Photos</a></li>
								<li><a href="#">Contact</a></li>
							</ul>
							
						</div><!-- .footer-nav -->

						<div class="footer-register">

							<span class="footer-register-title">Register Your School Today.</span>
							<a href="#" class="button primary fill">Register Now</a>

						</div><!-- .footer-register -->
					
					</div><!-- .footer-row -->
				</div><!-- .sw -->
				
				<div class="copyright">
					<div class="sw">

						<div class="copyright-box">
							<ul>
								<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">School Sports NL</a></li>
								<li><a href="#">Sitemap</a></li>
								<li><a href="#">Legal</a></li>
							</ul>
						</div><!-- .copyright-box -->
						
						<div class="copyright-box">
							<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac"><img src="../assets/images/jac-logo.svg" alt="JAC Logo."></a>
						</div><!-- .copyright-box -->

					</div><!-- .sw -->
				</div><!-- .copyright -->
				
			</footer><!-- .footer -->

		</div><!-- .page-wrapper -->	

		<script>
			var templateJS = {
				templateURL: 'http://dev.me/ssnl-codekit/templates',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>

		<script src="../assets/js/min/main-min.js"></script>
	</body>
</html>