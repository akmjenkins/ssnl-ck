<button class="toggle-nav">
	<span>&nbsp;</span> Menu
</button>

<div class="nav-wrap">

	<div class="sw">

		<nav>
			<ul>
				<li><a href="#">About</a></li>
				<li><a href="#">Schools</a></li>
				<li><a href="#">Sports</a></li>
				<li><a href="#">Resources &amp; Forms</a></li>
				<li><a href="#">Sports Stars</a></li>
				<li><a href="#">Photos</a></li>
				<li><a href="#">Contact</a></li>
			</ul>
		</nav>

	</div><!-- .sw -->

	<div class="nav-meta-bar">
		<div class="sw">

			<div class="nav-meta-nav">

				<div class="nav-meta-links">
					<a href="#" class="button primary fill">Login</a>
					<a href="#" class="button primary fill">Membership Registration</a>
				</div><!-- .nav-meta-links -->

				<div class="nav-meta-logo">
					<span>Go To</span>
					<a href="#"><img src="../assets/images/participation-nation-logo.svg" alt="Participation Nation Logo"></a>
				</div><!-- .nav-meta-logo -->

			</div><!-- .nav-meta-nav -->

		</div><!-- .sw -->
	</div><!-- .nav-meta-bar -->	

</div><!-- .nav-wrap -->