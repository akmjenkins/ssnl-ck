DIR=ssnl-codekit
ssh vhost1@162.244.30.109 -p 65535 rm -rf /var/www/projects/$DIR
tar -c --exclude=.git --exclude=sass/* --exclude=node_modules/* --exclude=src/* --exclude=Photoshop/* ../$DIR | gzip -2 | ssh vhost1@162.244.30.109 -p 65535 "tar -zx -C /var/www/projects/"
