;(function(context) {

	var debounce,tests;

	if(context) {
		debounce = context.debounce;
		tests = context.tests;
	} else {
		debounce = require('./debounce.js');
		tests = require('./tests.js');
	}

	var 
		methods,
		scrollDebounce = debounce(),
		resizeDebounce = debounce(),
		$window = $(window),
		$document = $(document),
		$html = $('html'),
		$body = $('body'),
		SHOW_NAV_CLASS = 'show-nav',
		SHOW_SEARCH_CLASS = 'show-search',
		SHOW_SMALL_NAV_CLASS = 'small-nav',
		COLLAPSE_NAV_AT = 850;

	methods = {
		
		checkShowSmallNav: function() {
			if(window.innerWidth > COLLAPSE_NAV_AT && $window.scrollTop() > 100) {
				$body.addClass(SHOW_SMALL_NAV_CLASS);
			} else {
				$body.removeClass(SHOW_SMALL_NAV_CLASS);
			}
		},
		
		isNavCollapsed: function() {
			return window.innerWidth < COLLAPSE_NAV_AT;
		},
		
		onScroll: function() {
			this.checkShowSmallNav();
		},
		
		onResize: function() {
			this.checkShowSmallNav();
		},
	
		showNav: function(show) {
			$html[show ? 'addClass' : 'removeClass'](SHOW_NAV_CLASS);
		},

		toggleNav: function() {
			this.showNav(!this.isShowingNav());
		},

		isShowingNav: function() {
			return $html.hasClass(SHOW_NAV_CLASS);
		},
		
		toggleSearch: function() {
			this.showSearch(!this.isShowingSearch());
		},
		
		showSearch: function(show) {
			$html[show ? 'addClass' : 'removeClass'](SHOW_SEARCH_CLASS);
		},
		
		isShowingSearch: function() {
			return $html.hasClass(SHOW_SEARCH_CLASS) && window.innerWidth > COLLAPSE_NAV_AT;
		}

	};
	
	//listeners
	$document
		.on('click','body',function(e) {
			$(e.target).hasClass('mobile-nav-bg') && methods.showNav(false);
		})		
		.on('click','.toggle-nav',function(e) {
			methods.toggleNav();
			return false;
		})
		.on('click','.toggle-search',function() {
			methods.toggleSearch()
		})
		.on('keydown',function(e) {
			if(e.result !== false && e.keyCode === 27) {
				
				if(methods.isShowingSearch()) {
					methods.showSearch(false);
					return false;
				}
				
				if(methods.isShowingNav()) {
					methods.showNav(false);
					return false
				}
			}
		});
		
	$window
		.on('scroll',function() { methods.onScroll(); })
		.on('resize',function() { methods.onResize(); });
		
		if(tests.ios()) {
			
			//onScroll must be fired continuously
			(function iOSOnScroll() {
				methods.onScroll();
				requestAnimationFrame(function() { iOSOnScroll(); });
			}());
		
		} else {
			//fire immediately
			methods.onScroll();
			methods.onResize();
		}
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));